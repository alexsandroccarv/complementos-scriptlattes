#!/usr/bin/python
# encoding: utf-8
#
# Jesús P. Mena-Chalco <jesus.mena@ufabc.edu.br>
#
# Atualização: Qua Mai  1 12:30:49 BRT 2013

import sys
import shutil
import os, errno
import fileinput
import urllib2
import time

# --------------------------------------------------------------------------------- #
# Programa principal
# --------------------------------------------------------------------------------- #
if __name__ == "__main__":
	inFile = sys.argv[1]
	http = "http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id="

	for line in fileinput.input(inFile):
		line = line.strip()
		id10 = line.split(',')

		if len(id10)>0:
			url = http + id10[0]

			#print "------------------------------------------"
			contador = 1
			while contador<=10:	
				try:
					req = urllib2.Request(url)
					arquivoH = urllib2.urlopen(req)
					cvLattesHTML = arquivoH.read()
					arquivoH.close()
					time.sleep(2)
				except:
					cvLattesHTML = ""

				if len(cvLattesHTML)>10:
					break
				print "#Tentando uma vez mais..."

				time.sleep(30)
				contador +=1

			texto = cvLattesHTML.split("\n")	
			for linha in texto:
				#print "=========="
				#print linha
				if "o para acessar este CV:" in linha:
					#print linha
	
					partes = linha.split("http://lattes.cnpq.br/")
					id16 = partes[1].strip("</li>")
					# print "http://lattes.cnpq.br/"+id16 +" , "+  id10[1]
					print id16 +" , "+  id10[1]
					

