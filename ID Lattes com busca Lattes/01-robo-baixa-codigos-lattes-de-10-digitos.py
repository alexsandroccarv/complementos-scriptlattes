#!/usr/bin/python
# encoding: utf-8
#
# Este programa permite extrair todo os codigos Lattes de 10 digitos
# i.e., todos os codigos que comecam com a letra K.
# Foi criado um outro script que permite converter os codigos de 10 digitos para
# os codigos de 16 digitos (os codigos utilizados pelo scriptLattes)
# 
#
# Jesús P. Mena-Chalco <jesus.mena@ufabc.edu.br>
# 
# Seg Abr  1 18:31:18 BRT 2013
#

import sys
import shutil
import os, errno
import fileinput
import Levenshtein
import numpy
import time
import urllib2
import re

# --------------------------------------------------------------------------------- #
# Programa principal
# --------------------------------------------------------------------------------- #
if __name__ == "__main__":
	numInicio = 0

	# Apenas as seguintes 2 variaveis devem ser configuradas
	numFim    = 3072
	urlBase = "http://buscatextual.cnpq.br/buscatextual/busca.do?metodo=forwardPaginaResultados&registros=10;10&query=%28%2Bidx_assunto%3Adengue+%2Bidx_participa_dgp%3A1++%2Bidx_particao%3A1+%2Bidx_nacionalidade%3Ae%29+or+%28%2Bidx_assunto%3Adengue+%2Bidx_participa_dgp%3A1++%2Bidx_particao%3A1+%2Bidx_nacionalidade%3Ab%29&analise=cv&tipoOrdenacao=null&paginaOrigem=index.do&mostrarScore=true&mostrarBandeira=true&modoIndAdhoc=null"
	
	#------------------------------------------------------------------------------------
	#------------------------------------------------------------------------------------
	# Nao precisa modificar as linhas restantes
	urlBase = urlBase.replace("&registros=10;10&","&")
	i = numInicio
	while i<numFim:
		url = urlBase + "&registros=" +str(i) + ";10&"
		#print url

		# Para cada pagina (contendo 10 pessoas)
		req       = urllib2.Request(url)
		arquivoH  = urllib2.urlopen(req)
		pagina    = arquivoH.read()
		arquivoH.close()
		time.sleep(10)
		
		regex           = re.compile(r"javascript:abreDetalhe(.*)\"")
		CVsPessoas      = regex.findall(pagina)
		lista10Pessoas  = set(CVsPessoas)

		for detalhe in lista10Pessoas:
			detalhe = detalhe.replace("'","")
			detalhe = detalhe.replace("(","")
			detalhe = detalhe.replace(")","")
			det = detalhe.split(',')
			endereco = det[0]
			nome     = det[1]
			print endereco +" , "+ nome
		i = i+10
