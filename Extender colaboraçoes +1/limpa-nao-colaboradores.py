#!/usr/bin/python
# encoding: utf-8
#
# Jesús P. Mena-Chalco <jmena@vision.ime.usp.br>
# Wed May 25 14:44:06 BRT 2011
#

import sys
import shutil
import os, errno
import pygraphviz
import Levenshtein
import fileinput
import numpy

def file_len(fmatriz):
	with open(fmatriz) as f:
		for i, l in enumerate(f):
			pass
	return i + 1

# --------------------------------------------------------------------------------- #
# Programa principal
# --------------------------------------------------------------------------------- #
if __name__ == "__main__":
	fList    = sys.argv[1]
	fMatriz  = sys.argv[2]

	N = file_len(fMatriz)
	matriz = numpy.zeros((N,N), dtype=numpy.float32)

	# ---------------------------------------------------------------------------- #
	# Leitura da matriz de adjacência
	# ---------------------------------------------------------------------------- #
	with open(fMatriz) as f:
		for i, linha in enumerate(f):
			vetor = linha.split()
			for j in range(0,N):
				matriz[i][j] = vetor[j]

	# ---------------------------------------------------------------------------- #
	# Leitura da lista
	# ---------------------------------------------------------------------------- #
	lista = []
	listaDepurada = []
	indicesProfessores = []
	indicesColaboradores = []
	
	i = 0
	for linha in fileinput.input(fList):
		linha = linha.replace("\r","")
		linha = linha.replace("\n","")
		lista.append(linha.strip())
	
		partes = linha.split(",")
	
		#if (partes[3].strip()=="Professor" or partes[3].strip()=="CMCC" or partes[3].strip()=="CECS" or partes[3].strip()=="CCNH"):
		#if (partes[3].strip()=="Membro"):
		#if (partes[3].strip()=="mac" or partes[3].strip()=="mae" or partes[3].strip()=="mat" or partes[3].strip()=="map"):
		#if (partes[3].strip()=="XXXX" or partes[3].strip()=="CMCC" or partes[3].strip()=="CECS" or partes[3].strip()=="CCNH"):
		#	indicesProfessores.append(i)
		if (partes[3].strip()=="Colaborador-Lattes"):
			indicesColaboradores.append(i)
		else:
			indicesProfessores.append(i)

		i = i+1

	# ---------------------------------------------------------------------------- #
	# inserimos todos os professores na lista "AS IS"
	for p in indicesProfessores:
		listaDepurada.append(p)

	# ---------------------------------------------------------------------------- #
	for c in indicesColaboradores:
		colaboradorColabora = False
		for p in indicesProfessores:
			if (matriz[c][p]>0):
				colaboradorColabora = True
		if (colaboradorColabora):
			listaDepurada.append(c)	
			
	# ---------------------------------------------------------------------------- #
	for x in listaDepurada:
		print lista[x]


