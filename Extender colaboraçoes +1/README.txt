
==========================================================================
junta-autores-e-colaboradores.py
==========================================================================

Script para juntar 2 listas de IDs Lattes. 
- A primeira lista é a lista de Pesquisadores. (Arquivo .list)
- A segunda lista é a lista dos colaboradores. (Arquivo gerado pelo scriptLattes após processar o arquivo .list. O arquivo tem o sufixo - colaboradores.txt)

É muito comum que um pesquisador da lista 1 esteja na lista 2. Assim, para
evitar redundancias dos IDs este script junta de forma unica as lista de IDs.
Os colaboradores aparecem com um novo rótulo "Colaborador-Lattes"


Sintaxe:
$ python junta-autores-e-colaboradores.py    <lista-de-autores-do-grupo>  <colaboradores-do-grupo>


Exemplo:
$ python junta-autores-e-colaboradores.py    teste-01.list    teste-01-colaboradores.txt


A saída será um arquivo como:   
teste-01-colaboradores-e-autores.txt




==========================================================================
limpa-nao-colaboradores.py
==========================================================================

A lista de colaboradores tratada pode ter pessoas que apenas nao fora
coautoras. Por exemplo pode ter pessoas orientadas. Assim, as vezes é
necessário ter apenas a lista dos que realmente colaboraram com o grupo de
autores (considerando o periodo).

Para o projeto de Doencas Negligenciadas esse programa não é útil.



==========================================================================

Qui Mai 30 21:28:30 BRT 2013
